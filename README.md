# 🥧 Gamarjoba JWT

## Usage
```ruby
require 'gamarjoba/jwt'
application = Doorkeeper::Application.find(ID)
Gamarjoba::JWT.decode(token, secret) # => [payload, header]
Gamarjoba::JWT.encode(application, secret) # => JWT token
Gamarjoba::JWT.encode(application, secret, payload: { custom: :payload })
Gamarjoba::JWT.get_kid(token, secret) # => application uid

# Долгоживущий токен можно сгенерить так: (из консоли)
Gamarjoba::JWT::TOKEN_DURATION = 86400 * 1000 # дней
Gamarjoba::JWT.encode(application, secret)
```




## Installation
Add this line to your application's Gemfile:

```ruby
gem 'gamarjoba-jwt', '0.0.4', git: 'https://gitlab.com/finservice-public/gamarjoba-jwt', tag: 'v0.0.4'
```

And then execute:
```bash
$ bundle
```

# TODO:
1. TESTS!

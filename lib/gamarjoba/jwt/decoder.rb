# frozen_string_literal: true

require 'jwt'
require 'gamarjoba/jwt/claims'

module Gamarjoba
  module JWT
    class Decoder
      attr_reader :secret

      def initialize(secret)
        @secret = secret
      end

      def call(token)
        ::JWT.decode(token, secret, true, iss: Claims::ISS, verify_iss: true, algorithm: ALGORITHM)
      end
      alias decode call

      def get_kid(token)
        _payload, headers = call(token)
        headers.fetch('kid')
      end
    end
  end
end

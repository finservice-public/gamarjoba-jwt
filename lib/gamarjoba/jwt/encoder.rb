# frozen_string_literal: true

require 'jwt'
require 'securerandom'
require 'gamarjoba/jwt/claims'

module Gamarjoba
  module JWT
    class Encoder
      attr_reader :application, :secret, :opts

      # TODO: Заменить application на uid, чтобы отвязать от реализации сервера
      # (потому что библиотека может использоваться не только на сервере).
      # Потребуется мажорная версия.
      def initialize(application, secret, **opts)
        @application = application
        @secret = secret
        @opts = opts
      end

      def call
        ::JWT.encode(token_payload, secret, Gamarjoba::JWT::ALGORITHM, token_headers)
      end
      alias encode call

      private

      def token_headers
        # TODO: перенести header['kid'] в payload['uid'], потому что kid служит для другого (Key ID).
        # Потребуется мажорная версия.
        { kid: application.uid }
      end

      def token_payload
        result = {
          iss: Claims::ISS,
          iat: Claims.iat,
          exp: Claims.exp,
          jti: Claims.jti
        }

        # According to https://www.iana.org/assignments/jwt/jwt.xhtml claim responsible for exposure of scopes is "scope"
        # but Doorkeeper's option is called "scopes"
        result[:scope] = opts[:scopes] unless opts[:scopes].nil? || opts[:scopes].empty?
        result[:exp] = result[:iat] + opts[:expires_in] unless opts[:expires_in].nil?

        result = opts[:payload].merge(result) if opts[:payload].is_a?(Hash)

        result
      end
    end
  end
end

# frozen_string_literal: true

module Gamarjoba
  module JWT
    module Claims
      # ISS генерируется так, чтобы ключи становились несовместимыми только между мажорными версиями.
      ISS = "Gamarjoba JWT v#{VERSION.split('.')[0]}.x.x"

      def self.jti
        SecureRandom.uuid
      end

      def self.iat
        Time.new.utc.to_i
      end

      def self.exp
        iat + Gamarjoba::JWT::TOKEN_DURATION
      end
    end
  end
end

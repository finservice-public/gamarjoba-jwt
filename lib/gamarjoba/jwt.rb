# frozen_string_literal: true

require 'gamarjoba/jwt/version'
require 'gamarjoba/jwt/decoder'
require 'gamarjoba/jwt/encoder'

module Gamarjoba
  module JWT
    ALGORITHM = 'HS512'
    TOKEN_DURATION = 86400

    def encode(application, secret, **opts)
      Encoder.new(application, secret, opts).encode
    end

    def decode(token, secret)
      Decoder.new(secret).decode(token)
    end

    def get_kid(token, secret)
      Decoder.new(secret).get_kid(token)
    end

    module_function :encode, :decode, :get_kid
  end
end
